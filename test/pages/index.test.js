import React from "react";
import { render, screen } from "@testing-library/react";
import Home from "../../pages/index";
import mockData from "../../mockData/data-copy.json";

describe("Home", () => {
	it("should render the heading", () => {
		render(<Home data={mockData} />);

		const heading = screen.getByText(/Dishwashers/);

		expect(heading).toBeInTheDocument();
	});

	it("correct number of dishwashers shown", () => {
		const { getAllByTestId } = render(<Home data={mockData} />);

		const dishwashers = getAllByTestId("product-item");

		expect(dishwashers.length).toBe(20);
	});
});
