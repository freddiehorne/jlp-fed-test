module.exports = {
	images: {
		domains: ["johnlewis.scene7.com"],
	},
	// insert lang="en" into html element
	i18n: {
		locales: ["en"],
		defaultLocale: "en",
	},
};
