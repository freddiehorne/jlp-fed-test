import Layout from "../components/layout/layout";
import "../styles/globals.scss";

function MyApp({ Component, pageProps }) {
	// What is netherworld???
	return (
		<Layout type="netherworld">
			<Component {...pageProps} />
		</Layout>
	);
}

export default MyApp;
