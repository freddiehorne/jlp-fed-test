import axios from "axios";
import { PRODUCT_URL } from "../../utils";
import ProductCarousel from "../../components/product-carousel/product-carousel";
import ProductInformation from "../../components/product-information/product-information";
import ProductSpecification from "../../components/product-specification/product-specification";
import Head from "next/head";

export async function getServerSideProps(context) {
	const id = context.params.id;
	const response = await axios.get(PRODUCT_URL + id);
	const data = await response.data;
	return {
		props: { data },
	};
}

const ProductDetail = ({ data }) => {
	if (!data) return <h1>Loading information...</h1>;
	return (
		<>
			<Head>
				<title>{data.title}</title>
				<meta name="keywords" content={data.title} />
				<meta
					name="description"
					content="Dishwasher images, description and specifications"
				/>
			</Head>
			<h1>{data.title}</h1>
			<ProductCarousel images={data.media.images.urls} />
			<ProductInformation
				price={data.price}
				displaySpecialOffer={data.displaySpecialOffer}
				productInformation={data.details.productInformation}
				includedServices={data.additionalServices.includedServices}
				code={data.code}
			/>
			<ProductSpecification data={data.details.features[0].attributes} />
		</>
	);
};

export default ProductDetail;
