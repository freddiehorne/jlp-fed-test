import axios from "axios";
import { MAIN_API_URL } from "../utils";
import Head from "next/head";
import Link from "next/link";
import ProductListItem from "../components/product-list-item/product-list-item";
import styles from "../styles/index.module.scss";

export async function getServerSideProps() {
	const response = await axios.get(MAIN_API_URL);
	const data = await response.data;
	return {
		props: { data },
	};
}

const Home = ({ data }) => {
	if (!data) return <h1>Loading page...</h1>;

	let products = data.products;
	// sort the price in descending order like in the design
	// "£" sign and comma are removed
	products.sort((product1, product2) => {
		const price1 = product1.variantPriceRange.display.max;
		const price2 = product2.variantPriceRange.display.max;
		const first = price1.slice(1, price1.length).replace(",", "");
		const second = price2.slice(1, price2.length).replace(",", "");
		return second - first;
	});
	// display first 20 dishwashers in order of descending price
	const items = products.slice(0, 20);

	return (
		<>
			<Head>
				<title>JL &amp; Partners | Home</title>
				<meta name="keywords" content="shopping, dishwasher, dishwashers" />
				<meta
					name="description"
					content="A display of available dishwashers in order of descending price"
				/>
			</Head>
			<h1 className={styles.heading}>Dishwashers ({items.length})</h1>
			<section className={styles.content}>
				{items.map((item) => (
					<Link
						key={item.productId}
						href={{
							pathname: "/product-detail/[id]",
							query: { id: item.productId },
						}}
					>
						<a className={styles.link} data-testid="product-item">
							<ProductListItem
								image={item.image}
								title={item.title}
								price={item.variantPriceRange.display.max}
							/>
						</a>
					</Link>
				))}
			</section>
		</>
	);
};

export default Home;
