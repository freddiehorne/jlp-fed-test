<!-- # UI Dev Technical Test - Dishwasher App

## Brief

Your task is to create a website that will allow customers to see the range of dishwashers John Lewis sells. This app will be a simple to use and will make use of existing John Lewis APIs.

We have started the project, but we'd like you to finish it off to a production-ready standard. Bits of it may be broken.

### Product Grid

When the website is launched, the customer will be presented with a grid of dishwashers that are currently available for customers to buy.

For this exercise we’d be looking at only displaying the first 20 results returned by the API.

### Product Page

When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details.

### Designs

In the `/designs` folder you will find 3 images that show the desired screen layout for the app

- product-page-portrait.png
- product-page-landscape.png
- product-grid.png

### Mock Data

There is mock data available for testing in the `mockData` folder.

## Things we're looking for

- Unit tests are important. We’d like to see a TDD approach to writing the app. We've included a Jest setup.
- The website should be fully responsive, working across device sizes. We've provided you with some ipad-sized images as a guide.
- The use of third party code/SDKs is allowed, but you should be able to explain why you have chosen the third party code.
- Put all your assumptions, notes, instructions and improvement suggestions into your GitHub README.md.
- We’re looking for a solution that's as close to the designs as possible.
- We'll be assessing your coding style, how you've approached this task and whether you've met quality standards on areas such as accessibility, security and performance.
- We don't expect you to spend too long on this, as a guide 3 hours is usually enough.

---

## Getting Started

- `Fork this repo` into your own GitLab namespace (you will need a GitLab account).
- Install the NPM dependencies using `npm i`. (You will need to have already installed NPM using version `14.x`.)
- Run the development server with `npm run dev`
- Open [http://localhost:3000](http://localhost:3000) with your browser. -->

### Third party

- axios
- nanoid - to generate random keys for map methods without relevant data
- react-icons - for the two arrows in product-carousel
- react-show-more-text - to help with hiding most of the product information data
- isomorphic-dompurify - although I'm sure this data is safe I have learnt that it is good practice to sanitise when using `dangerouslySetInnerHTML`
- babel-jest
- @testing-library/react
- @testing-library/jest-dom
- identity-obj-proxy

This was my first time working with Next.JS and while I really enjoyed learning about it and I think I have a pretty good understanding of it. I was confused by the `type="netherworld"` attribute in the `_app.js` page. I couldn't find any information on it hence my comment...

I have used Axios instead of fetch. This is mainly because it is what I know and have used a lot

I have moved any `.scss` files for pages into the styles directory but left `.scss` files for components in their respective component directories. This made the most sense to me. I have also added some very basic styles to the 404 page to show it in the middle of the viewport

`react-show-more-text` and `react-icons` were installed for the product-information and product-carousel components respectively

I have used nano id to create individual keys for the map methods in the `product carousel` and `product-specification` components as I couldn't find relevant data

I'm aware of packages like `pure-react-carousel` but I created my own basic carousel as I knew it wouldn't take too long and was a good show case of skill

This was my first time with the practical implementation of testing in a project. I gave it my best shot but as you will see I came across some problems...
`babel-jest` installed as this app is using Next x10 and doesn't have the rust compiler built in. The other three testing dependencies were installed as per the Next.JS docs

The structure of `data.json` does not match the real data so I copied the data from the API call for testing `index.jsx`

### Improvements

- Intrinsic image size is quite large (1800 x 2400 in some cases). Although Next.JS Image component optimizes images perhaps speed could be improved if the image was smaller to begin with?
- Images could be cropped or given a transparent background
