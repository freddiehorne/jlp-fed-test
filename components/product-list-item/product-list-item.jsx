import Image from "next/image";
import styles from "./product-list-item.module.scss";

const ProductListItem = ({ image, title, price }) => {
	return (
		<>
			<div className={styles.image}>
				<Image
					src={`https:${image}`}
					alt={title}
					width={280}
					height={375}
					quality={50}
					layout="intrinsic"
				/>
			</div>
			<p className={styles.title}>{title}</p>
			<p className={styles.price}>{price}</p>
		</>
	);
};

export default ProductListItem;
