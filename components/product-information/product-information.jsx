import ShowMoreText from "react-show-more-text";
import DOMPurify from "isomorphic-dompurify";
import styles from "./product-information.module.scss";

const ProductInformation = ({
	price,
	displaySpecialOffer,
	includedServices,
	productInformation,
	code,
}) => {
	//sanitise the html before it is rendered to protect against XSS
	const clean = DOMPurify.sanitize(productInformation);

	return (
		<section className={styles.content}>
			<p>
				<strong className={styles.price}>£{price.now}</strong>
			</p>
			<p className={styles.specialOffer}>{displaySpecialOffer}</p>
			<p className={styles.includedServices}>{includedServices}</p>
			<h3>Product information</h3>
			<ShowMoreText lines={4}>
				<div
					dangerouslySetInnerHTML={{
						__html: clean,
					}}
					data-testid="product-description"
				/>
			</ShowMoreText>
			<p className={styles.code}>Product code: {code}</p>
		</section>
	);
};

export default ProductInformation;
