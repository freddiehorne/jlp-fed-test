import React from "react";
import { render, fireEvent } from "@testing-library/react";
import ProductInformation from "../product-information";
import { detailsData } from "../../../mockData/data2.json";

const testProduct = detailsData[0];

// These two tests fail for a reason I don't understand "ReferenceError: TextEncoder is not defined" but I left them to show how I would test this component
describe("ProductInformation", () => {
	test("has an heading with the price", async () => {
		const { getByText } = render(<ProductInformation {...testProduct} />);

		expect(getByText(`£${testProduct.price.now}`)).toBeInTheDocument();
	});

	it("has working expansion panels", async () => {
		const { getByText } = render(<ProductInformation {...testProduct} />);

		const element = screen.getByTestId("product-description");
		expect(element).toBeInTheDocument();
		const showMore = getByText("Show more");
		expect(showMore).toBeInTheDocument();

		const targetText = "dosageAssist";
		const expansionPanelContent = getByText(targetText);

		expect(expansionPanelContent).not.toBeInTheDocument();

		fireEvent.click(showMore);

		expect(expansionPanelContent).toBeInTheDocument();
	});
});
