import React from "react";
import { render, screen } from "@testing-library/react";
import ProductSpecification from "../product-specification";
import mockData from "../../../mockData/data2.json";

const data = Object.keys(mockData.detailsData[0].dynamicAttributes);

describe("ProductSpecification", () => {
	test("if number of rendered attributes matches number in data", () => {
		const { getAllByRole } = render(<ProductSpecification data={data} />);

		const specifics = getAllByRole("listitem");

		expect(specifics.length).toEqual(data.length);
	});
});
