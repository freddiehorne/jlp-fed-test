import { nanoid } from "nanoid";
import styles from "./product-specification.module.scss";

const ProductSpecification = ({ data }) => {
	return (
		<section className={styles.content}>
			<h3 className={styles.heading}>Product specification</h3>
			<ul>
				{data.map((item) => (
					<li className={styles.list} key={nanoid()}>
						<dl className={styles.container}>
							<dt>{item.name}</dt>
							<dd>{item.value}</dd>
						</dl>
					</li>
				))}
			</ul>
		</section>
	);
};

export default ProductSpecification;
