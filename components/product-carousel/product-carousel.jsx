import { useState } from "react";
import { AiOutlineArrowLeft, AiOutlineArrowRight } from "react-icons/ai";
import { nanoid } from "nanoid";
import Image from "next/image";
import styles from "./product-carousel.module.scss";

const ProductCarousel = ({ images }) => {
	const [currentSlide, setCurrentSlide] = useState(0);
	const length = images.length;

	const prevSlide = () =>
		setCurrentSlide(currentSlide === 0 ? length - 1 : currentSlide - 1);
	const nextSlide = () =>
		setCurrentSlide(currentSlide === length - 1 ? 0 : currentSlide + 1);
	// keyboard functionality for arrows when tabbed
	const onEnterLeft = (e) => {
		if (e.key === "Enter") {
			prevSlide();
		}
	};
	const onEnterRight = (e) => {
		if (e.key === "Enter") {
			nextSlide();
		}
	};

	return (
		<section className={styles.carousel}>
			<AiOutlineArrowLeft
				className={styles.leftArrow}
				onClick={prevSlide}
				onKeyDown={onEnterLeft}
				tabIndex={0}
				role="button"
			/>
			<AiOutlineArrowRight
				className={styles.rightArrow}
				onClick={nextSlide}
				onKeyDown={onEnterRight}
				tabIndex={0}
				role="button"
			/>
			{images.map(
				(imageURL, i) =>
					currentSlide === i && (
						<Image
							src={`https:${imageURL}`}
							alt={`Alternate ${i + 1}`}
							width={350}
							height={465}
							quality={50}
							layout="intrinsic"
							key={nanoid()}
							className={styles.image}
						/>
					)
			)}
		</section>
	);
};

export default ProductCarousel;
