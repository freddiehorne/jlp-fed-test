import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import ProductCarousel from "../product-carousel";
import { AiOutlineArrowLeft } from "react-icons/ai";
import mockData from "../../../mockData/data2.json";

const data = mockData.detailsData[0];

describe("ProductCarousel", () => {
	test("number of arrows is 2", () => {
		const { getAllByRole } = render(
			<ProductCarousel images={data.media.images.urls} />
		);

		const arrows = getAllByRole("button");

		expect(arrows.length).toEqual(2);
	});

	// I'm aware this test doesn't pass but I wanted to have a go a writing it and show how I would go about it
	test("calls onClick prop when clicked", async () => {
		const handleClick = jest.fn();

		render(<AiOutlineArrowLeft onClick={handleClick} />);

		fireEvent.click(await screen.findByRole("button", { hidden: true }));

		expect(handleClick).toHaveBeenCalledTimes(1);
	});
});
