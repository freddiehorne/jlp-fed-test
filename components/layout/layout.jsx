const Layout = ({ children }) => {
	return <main>{children}</main>;
};

export default Layout;

// I removed all the extra divs as I didn't see the need for them and they cluttered up the DOM tree
