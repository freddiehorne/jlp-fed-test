export const MAIN_API_URL = `https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=${process.env.API_KEY}`;

export const PRODUCT_URL =
	"https://api.johnlewis.com/mobile-apps/api/v1/products/";
